package com.golf.service;

import java.util.List;

import com.golf.domain.Golfer;

public interface MemberService {
	
	public List<Golfer> getMembers();
	public Golfer getMember(long id);
	public void createMember(String name);
}
