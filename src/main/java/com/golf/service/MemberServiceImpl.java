package com.golf.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.golf.domain.Golfer;
import com.golf.repostiory.MemberRepository;

@Service
public class MemberServiceImpl implements MemberService {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	MemberRepository repository;
	
	@Override
	public List<Golfer> getMembers() {
		List<Golfer> members = repository.findAll();
		return members;
	}

	@Override
	public Golfer getMember(long id) {
		Optional<Golfer> member = repository.findById(id);//.orElseGet(this::getDefaultValue));
		
		return member.orElse(getDefaultValue());
	}
	@Override
	public void createMember(String name) {
		Golfer member = new Golfer(name);
		repository.save(member);
	}

	public Golfer getDefaultValue() {
	
	    Golfer defaultGolfer = new Golfer("Golfer Not Found");
	
	    return defaultGolfer;
	
	}


//	}
	
//	@Override
//	public void run(String... args) throws Exception {
//
//		logger.info("Student id 10001 -> {}", repository.findById(10001L));
//
//		logger.info("Inserting -> {}", repository.save(new Student("John", "A1234657")));
//
//		logger.info("Update 10003 -> {}", repository.save(new Student(10001L, "Name-Updated", "New-Passport")));
//
//		repository.deleteById(10002L);
//
//		logger.info("All users -> {}", repository.findAll());
//	}

}
