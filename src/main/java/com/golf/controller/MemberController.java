package com.golf.controller;

import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.golf.domain.Golfer;
import com.golf.service.MemberService;

@RestController
public class MemberController {
	
	@Autowired
	MemberService memberService;
	int id;
	
	@GetMapping("/")
	public ResponseEntity<List<Golfer>> getMembers() {
		return ResponseEntity.ok(memberService.getMembers());
	}
	
	@GetMapping(path = "/member")
	public ResponseEntity<Golfer> getMember(@RequestParam(value = "id") int id) {
		
		return ResponseEntity.ok(memberService.getMember(id));
	}
	
	@PostMapping(path = "/")
	@ResponseStatus(HttpStatus.OK)
		public void createMember(@RequestParam(value= "name") String name) {
			memberService.createMember(name);
		}

}
