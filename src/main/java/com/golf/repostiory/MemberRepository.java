package com.golf.repostiory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.golf.domain.Golfer;

public interface MemberRepository extends JpaRepository<Golfer, Long> {

}
