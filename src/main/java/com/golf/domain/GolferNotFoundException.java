package com.golf.domain;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class GolferNotFoundException extends RuntimeException{
	

	public GolferNotFoundException(String exception) {
		super(exception);
	}

}
