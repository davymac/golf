package com.golf.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Setter
@Getter
@ToString
@EqualsAndHashCode
@Entity
public class Golfer {
	
	@Id
	@GeneratedValue
	private long id;
	private final String name;
	
	public Golfer() {
		this.name = "";
	}
	
	public Golfer(String name){
		super();
		this.name = name;
	}

}
