import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
public class CorsConfiguration implements WebMvcConfigurer {
	
    /**
     * Enables Cross-Origin Resource Sharing (CORS)
     * More info: http://docs.spring.io/spring/docs/current/spring-framework-reference/html/cors.html
     * @param registry
     */
    @Override
    public void addCorsMappings(final CorsRegistry registry) {
        registry.addMapping("/**");
    }
    
    @Bean
    public WebMvcConfigurer corsConfigurer() {
    	
    	return new WebMvcConfigurer() {
    	@Override
    	public void addCorsMappings(final CorsRegistry registry) {
            registry.addMapping("/**");//.allowedOrigins("http://localhost:<add port here>");
            }
    	};
    }

}
