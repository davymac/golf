package com.golf;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.golf.domain.Golfer;

@SpringBootTest
class GolfApplicationTests {

	@Test
	void contextLoads() {
	}
	
	@Disabled
	@Test 
	public void getAllGolfersTest()
	{
		Golfer member1 = new Golfer("David");
		Golfer member2 = new Golfer("Karen");
		
		List <Golfer> members = new ArrayList<Golfer>();
		members.add(member1);
		members.add(member2);
		
		//fail("not yet implemented");
	}
	
	@Test 
	public void createGolferTest()
	{
		Golfer member1 = new Golfer("David");
		
		assertEquals(member1.getName(), "David");

	}
	
}
